from __future__ import annotations

import json
from typing import TYPE_CHECKING

from mastodon import Mastodon
from mastodon.Mastodon import MastodonMalformedEventError
import six
import websockets

if TYPE_CHECKING:
    from pleroma import Pleroma


class StreamListener:
    """Callbacks for the streaming API. Create a subclass, override the on_xxx
    methods for the kinds of events you're interested in
    """

    def __init__(
        self,
        pleroma: Pleroma,
        stream: str,  # 'user'|'public'|'public:local'|'hashtag'|'hashtag:local'|'list'|'direct'
    ):
        self.pleroma = pleroma
        self.stream = stream

    async def handle_events(self) -> None:
        access_token = self.pleroma.access_token
        api_base = f"{self.pleroma.instance()['urls']['streaming_api']}"
        uri = f"{api_base}/api/v1/streaming?access_token={access_token}&stream={self.stream}"
        async with websockets.connect(uri) as websocket:
            async for message in websocket:
                self._dispatch(message)

    def _dispatch(self, message) -> None:
        try:
            message = json.loads(message)
            event = message["event"]
            payload = message["payload"]
            if event in ["update", "notification"]:
                payload = json.loads(payload, object_hook=Mastodon._Mastodon__json_hooks)
        except KeyError as err:
            exception = MastodonMalformedEventError("Missing field", err.args[0], message)
            six.raise_from(exception, err)
        except ValueError as err:
            exception = MastodonMalformedEventError("Bad JSON", payload)
            six.raise_from(exception, err)
        handler_name = f"on_{event}"
        try:
            handler = getattr(self, handler_name)
        except AttributeError as err:
            exception = MastodonMalformedEventError("Bad event type", event)
            six.raise_from(exception, err)
        handler(payload)

    def on_update(self, status) -> None:
        """A new status has appeared! 'status' is the parsed JSON dictionary
        describing the status."""
        pass

    def on_notification(self, notification) -> None:
        """A new notification. 'notification' is the parsed JSON dictionary
        describing the notification."""
        pass

    def on_delete(self, status_id: str) -> None:
        """A status has been deleted. status_id is the status' integer ID."""
        pass


class EventHandler(StreamListener):
    """
    Simple callback stream handler class.
    """

    def on_update(self, status) -> None:
        print("update")
        print(status)

    def on_delete(self, status_id) -> None:
        print("delete")
        print(status_id)

    def on_notification(self, notification) -> None:
        print("notification")
        print(notification)
