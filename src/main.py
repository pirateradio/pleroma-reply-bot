import asyncio

from event_handler import EventHandler
from pleroma import Pleroma


def main() -> None:
    pleroma = Pleroma(email="*", password="*", api_base_url="https://pirateradio.social")
    event_handler = EventHandler(pleroma=pleroma, stream="public:local")

    asyncio.get_event_loop().run_until_complete(event_handler.handle_events())


if __name__ == "__main__":
    main()
