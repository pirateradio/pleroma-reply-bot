import os
from typing import Optional

from mastodon import Mastodon

from helpers import get_relative_path


class Pleroma(Mastodon):
    def __init__(self, email: Optional[str], password: Optional[str], api_base_url: str):
        # Create app if doesn't exist
        if not os.path.isfile(get_relative_path("../app.secret")):
            print("Creating app")
            Mastodon.create_app(
                "ReplyBot", to_file=get_relative_path("../app.secret"), api_base_url=api_base_url
            )
        # Fetch access token if I didn't already
        if not os.path.isfile(get_relative_path("../app_user.secret")):
            if email is None or password is None:
                raise Exception(
                    "email and password are required if an access token has not been saved"
                )
            print("Logging in")
            super().__init__(
                client_id=get_relative_path("../app.secret"),
                api_base_url=api_base_url,
                feature_set="pleroma",
            )
            super().log_in(email, password, to_file=get_relative_path("../app_user.secret"))
        else:
            super().__init__(
                client_id=get_relative_path("../app.secret"),
                access_token=get_relative_path("../app_user.secret"),
                api_base_url=api_base_url,
                feature_set="pleroma",
            )
